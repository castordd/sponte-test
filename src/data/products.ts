export const PRODUCTS: I.ProductProps[] = [
  {
    id: 2345,
    quantity: 7,
    title: 'NOVENTA STRATOCASTER®',
    slug: 'noventa-stratocaster',
    shortDescription:
      'Combining classic Fender style and dynamic single-coil pickups, the Noventa series delivers powerful tones, modern playability and dashing good looks.',
    description: `<p>Combining classic Fender style and dynamic single-coil pickups, the Noventa series delivers powerful tones, modern playability and dashing good looks.</p>
    <p>The Noventa Stratocaster® is an authoritative tone machine – featuring Noventa single-coil pickups, master volume and tone controls, and a hardtail Strat bridge – its striking good looks are matched by the midrange bite, crisp highs and smooth lows of the Noventa pickups. The dynamic range of these pickups offers a wide array of versatile and potent tones – suitable for everything from hard rock to jazz and anything in-between. A Modern “C” neck with 21 medium jumbo frets and 9.5” radius fingerboard delivers a smooth blend of modern and vintage playability that is distinctly Fender.</p>`,
    sizes: {
      width: 95,
      heigth: 45,
      deep: 5
    },
    weigth: 3.5,
    code: '0140923338',
    barCode: 'M40923338',
    category: 'Stratocaster',
    price: 1559,
    dateInit: '2017-07-23',
    imgUrl: '/images/red-apple/0140923338.jpeg',
    imgMini: '/images/red-apple/0140923338_m.jpeg'
  },
  {
    id: 2348,
    quantity: 3,
    title: 'AMERICAN PROFESSIONAL II TELECASTER®',
    slug: 'america-professional-ii-telecaster',
    shortDescription:
      'The American Professional II Telecaster® draws from more than seventy years of innovation, inspiration and evolution to meet the demands of today’s working player.',
    description: `<p>The American Professional II Telecaster® draws from more than seventy years of innovation, inspiration and evolution to meet the demands of today’s working player.</p>
    <p>Our popular Deep "C” neck now sports smooth rolled fingerboard edges, a “Super-Natural” satin finish and a newly sculpted neck heel for a supremely comfortable feel and easy access to the upper register. New V-Mod II Telecaster single-coil pickups are more articulate than ever while delivering the twang, snap and snarl that made the Tele famous. The new top-load/string-through bridge with compensated “bullet” saddles is our most comfortable, flexible Tele bridge yet – retaining classic brass-saddle tone and providing excellent intonation and flexible setup options, allowing you to tailor the tension and tone of each string to your liking.</p>
    <p>The American Pro II Telecaster delivers instant familiarity and sonic versatility you’ll feel and hear right away, with broad ranging improvements that add up to nothing less than a new standard for professional instruments.</p>`,
    sizes: {
      width: 96.3,
      heigth: 44,
      deep: 4.5
    },
    weigth: 3.5,
    code: '0113940761',
    barCode: 'M13940761',
    category: 'Telecaster',
    price: 2329,
    dateInit: '2018-08-28',
    imgUrl: '/images/deep-blue/0113940761.jpeg',
    imgMini: '/images/deep-blue/0113940761_m.jpeg'
  },
  {
    id: 2376,
    quantity: 9,
    title: 'JIM ROOT JAZZMASTER®',
    slug: 'jim-root-jazzmaster',
    shortDescription: `Stark, dark and menacing, the crushing Jim Root Jazzmaster guitar has got to be the most distinctively minimalist version of the instrument ever devised in the model's entire half-century history`,
    description: `<p>Stark, dark and menacing, the crushing Jim Root Jazzmaster guitar has got to be the most distinctively minimalist version of the instrument ever devised in the model's entire half-century history. At the behest of the towering Slipknot guitarist, gone are the dual tone circuits and barrage of controls. Gone are the fret position markers and enormous chrome bridge. Gone, in fact, is pretty much everything typical of a Jazzmaster, replaced only by fearsome EMG® humbucking pickups with brushed nickel covers, single three-way switch and single volume knob, a hardtail bridge and little else.</p>`,
    sizes: {
      width: 94.5,
      heigth: 45,
      deep: 4.8
    },
    weigth: 3.5,
    code: '0115300706',
    barCode: 'M15300706',
    category: 'Jazzmaster',
    price: 2329,
    dateInit: '2017-07-27',
    imgUrl: '/images/black-smook/0115300706.jpeg',
    imgMini: '/images/black-smook/0115300706_m.jpeg'
  }
]
