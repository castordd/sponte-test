import '@/styles/globals'
import Head from 'next/head'
import { useRouter } from 'next/router'
import { NavNemu } from '../components'
import GlobalStyles from '../styles/globals'
import { PRODUCTS } from '../data/products'

function MyApp({ Component, pageProps }) {
  const router = useRouter()
  const { slug } = router.query

  const productData = PRODUCTS.filter((prod) => prod.slug === slug)
  const product = productData[0]

  const description =
    'Test para uma vaga como desenvolvedor FrontEnd na empresa SPONTE'

  return (
    <>
      <Head>
        <title>
          SPONTE - {product === undefined ? 'FrontEnd [test]' : product.title}
        </title>
        <meta
          name="description"
          content={
            product === undefined ? description : product.shortDescription
          }
        />
      </Head>
      <GlobalStyles />
      <NavNemu />
      <Component {...pageProps} />
    </>
  )
}

export default MyApp
