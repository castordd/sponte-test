import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import parser from 'html-react-parser'
import { useBarcode } from 'react-barcodes'

import { AlertDog, BtnBack } from 'components'

import { PRODUCTS } from 'data/products'

import {
  Category,
  Container,
  BoxImage,
  Img,
  BoxData,
  DataItem,
  Title,
  Price,
  BtnEdit,
  EditLink
} from './styles'

const Product = () => {
  const router = useRouter()
  const { slug } = router.query
  const [isMobile, setIsMobile] = useState(false)

  const productData = PRODUCTS.filter((prod) => prod.slug === slug)
  const product = productData[0]

  useEffect(() => {
    if (window.innerWidth <= 480) {
      setIsMobile(true)
    }
  }, [])

  const BarCode = ({ code }) => {
    const { inputRef } = useBarcode({
      value: code,
      options: {
        format: 'CODE39',
        width: 1.7,
        height: 50
      }
    })

    return <svg ref={inputRef} />
  }

  return (
    <Container>
      {productData.length > 0 ? (
        <>
          <BoxImage>
            <Img
              alt={product?.title}
              src={product?.imgUrl}
              width={700}
              height={200}
              loading="lazy"
            />
            {isMobile && <Category>{product.category}</Category>}
          </BoxImage>
          <BoxData>
            <DataItem>
              <Title>
                {product?.title || '---'}
                <small># {product?.code || '---'}</small>
                {!isMobile && <span>{product.category}</span>}
              </Title>
            </DataItem>
            <DataItem>{parser(product.description)}</DataItem>
            <DataItem>
              <span>
                Width: <strong>{product.sizes.width} cm</strong>
              </span>
              <span>
                Heigth: <strong>{product.sizes.heigth} cm</strong>
              </span>
              <span>
                Deep: <strong>{product.sizes.deep} cm</strong>
              </span>
            </DataItem>
            <DataItem>
              <span>
                Weigth: <strong>{product.weigth} kg</strong>
              </span>
            </DataItem>
            <DataItem>
              <span>
                Bar code: <strong>{product.barCode}</strong>
              </span>
              <BarCode code={product.barCode} />
            </DataItem>
            <DataItem>
              <Price>
                Stock <strong>{product.quantity}</strong> - Price: US${' '}
                {product.price}
              </Price>
            </DataItem>
            <DataItem>
              <BtnEdit href={`/product/edit/${product.slug}`}>
                <EditLink>EDITAR</EditLink>
              </BtnEdit>
            </DataItem>
          </BoxData>
          <BtnBack url="/" />
        </>
      ) : (
        <AlertDog />
      )}
    </Container>
  )
}

export default Product
