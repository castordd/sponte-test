import { useRouter } from 'next/router'
import FormEdit from './Form'

import { AlertDog, BtnBack } from 'components'

import { PRODUCTS } from '../../../data/products'
import { Container, TitleSection } from './styles'

const ProductEdit = () => {
  const router = useRouter()
  const { slug } = router.query

  const productData = PRODUCTS.filter((prod) => prod.slug === slug)
  const product = productData[0]

  return (
    <>
      {product ? (
        <Container>
          <TitleSection>Editar o Produto {product.title}</TitleSection>
          <FormEdit product={product} />
          <BtnBack url={`/product/${slug}`} />
        </Container>
      ) : (
        <AlertDog />
      )}
    </>
  )
}

export default ProductEdit
