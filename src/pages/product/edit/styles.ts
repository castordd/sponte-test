import styled from 'styled-components'
import Image from 'next/image'

export const Container = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  max-width: 1200px;
  padding: 2rem 1rem;
  margin: 0 auto;
  border-radius: 0 0 9px 9px;
  border: 1px solid #eee;
  border-top: none;
  background-color: #f1f1f1;
  @media (min-width: 480px) {
    /* flex-direction: row-reverse; */
  }
`
export const TitleSection = styled.h1``

export const FormProduct = styled.form`
  width: fit-content;
  padding: 2rem 0 0;
`
export const RowForm = styled.div`
  display: flex;
  border-bottom: 1px solid #ccc;
  padding: 0 0 1rem;
  flex-direction: column;
  @media (min-width: 480px) {
    flex-direction: row;
  }
`

export const ColForm = styled.div`
  flex: 1;
  padding: 0 1rem;
`

export const FormControl = styled.div`
  width: 100%;
  padding: 0.5rem 0;

  label {
    display: block;
    color: #666;
    font-size: 12px;
    text-transform: capitalize;
    padding: 0 0 0.3rem 1rem;
    &.error {
      color: #f44;
    }
  }

  input {
    color: #666;
    width: 100%;
    border: 1px solid #ccc;
    padding: 0.5rem 1rem;
    border-radius: 6px;
    &.error {
      border: 1px solid #f44 !important;
    }
  }

  textarea {
    color: #666;
    width: 100%;
    display: block;
    min-height: 150px;
    border: 1px solid #ccc;
    padding: 0.5rem 1rem;
    border-radius: 6px;
  }
  .msnError {
    color: #f44;
    font-size: 12px;
    padding: 0.3rem 0.5rem;
  }
`
export const ImgMini = styled(Image)`
  display: inline-block !important;
  padding: 0.5rem !important;
  border-radius: 6px;
  background-color: #fff;
  margin: 0 0 0.5rem 0 !important;
`

export const BtnSubmit = styled.button`
  display: block;
  color: rgba(0, 0, 0, 0.7);
  font-size: 16px;
  font-weight: bold;
  padding: 0.5rem 1rem;
  margin: 1rem auto 0;
  border-radius: 6px;
  background-color: #785afd;
  border: none;
  transition: all ease-in-out 300ms;
  cursor: pointer;

  &:disabled {
    background-color: #ccc;
    cursor: not-allowed;
  }

  &:hover:not(:disabled) {
    color: rgba(255, 255, 255, 0.8);
    background-color: #6d52e3;
    box-shadow: 0 8px 24px -9px #000;
  }
`
