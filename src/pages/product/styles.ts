import styled from 'styled-components'

import Image from 'next/image'
import Link from 'next/link'

export const Container = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  max-width: 1200px;
  padding: 2rem 1rem;
  margin: 0 auto;
  border-radius: 0 0 9px 9px;
  border: 1px solid #eee;
  border-top: none;
  background-color: #f1f1f1;
  @media (min-width: 480px) {
    flex-direction: row-reverse;
  }
`
export const BoxImage = styled.div`
  position: relative;
  width: 100%;
  padding: 1rem;
  margin: 0 0 1rem 0;
  border-radius: 9px;
  background-color: #fff;
  @media (min-width: 480px) {
    width: 90%;
  }
`
export const Img = styled(Image)`
  width: 100%;
  height: inherit;
`

export const BoxData = styled.ul`
  width: 100%;
  max-width: 400px;
  display: inline-block;
  list-style: none;
  @media (min-width: 480px) {
    padding: 0 1rem;
  }
`

export const DataItem = styled.li`
  padding: 1rem;
  border-bottom: 1px solid #ccc;
  p {
    color: #666;
    font-size: 12px;
    line-height: 1.3;
    padding: 0 0.5rem;
    margin: 0 0 1rem 0;
  }
  span {
    color: #666;
    font-size: 14px;
    padding: 0 0.5rem;
    & + svg {
      display: block;
      margin: 0.5rem auto 0 auto;
    }
  }
`

export const Title = styled.h1`
  font-size: 1.3rem;
  margin: 0 0 0 0;
  small {
    color: #666;
    display: inline-block;
    width: 100%;
    font-size: 1rem;
  }
`

export const Category = styled.span`
  color: #666;
  width: 100%;
  display: inline-block;
  font-size: 12px;
  text-align: right;
`

export const Price = styled.span``

export const BtnEdit = styled(Link)``

export const EditLink = styled.a`
  display: inline-block;
  width: 100%;
  color: #000;
  font-size: 1.5rem;
  font-weight: bold;
  text-align: center;
  padding: 1rem 0;
  border-radius: 9px;
  background-color: #785afd;
  cursor: pointer;
  transition: all ease-in-out 300ms;

  &:hover {
    color: rgba(255, 255, 255, 0.8);
    background-color: #6d52e3;
    box-shadow: 0 8px 24px -9px #000;
  }
`
