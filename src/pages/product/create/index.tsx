import { BtnBack } from '@/components'
import FormCreate from './Form'

import { Container, TitleSection } from './styles'

const ProductEdit = () => {
  return (
    <Container>
      <TitleSection>Registrar um novo Produto</TitleSection>
      <FormCreate />
      <BtnBack url="/" />
    </Container>
  )
}

export default ProductEdit
