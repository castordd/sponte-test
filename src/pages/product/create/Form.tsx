import { Formik, Field } from 'formik'
import * as Yup from 'yup'

import { TextField, TextArea } from 'components'

import { FormControl, RowForm, ColForm, FormProduct, BtnSubmit } from './styles'

const FormCreate = () => {
  const DateToDay = () => {
    const data = new Date()
    const dia = data.getDate().toString()
    const diaF = dia.length == 1 ? '0' + dia : dia
    const mes = (data.getMonth() + 1).toString()
    const mesF = mes.length == 1 ? '0' + mes : mes
    const anoF = data.getFullYear()

    return `${diaF}-${mesF}-${anoF}`
  }

  const validate = Yup.object().shape({
    id: Yup.number().min(4, 'Minimo 4 digitos').required('Campo Obrigatório'),
    quantity: Yup.number()
      .min(1, 'Minimo 1 Produto')
      .required('Campo Obrigatório'),
    title: Yup.string()
      .min(10, 'Minino 10 caracteres')
      .max(100, 'Maximo 100 caracteres')
      .required('Campo Obrigatório'),
    slug: Yup.string()
      .min(10, 'Minino 10 caracteres')
      .required('Campo Obrigatório'),
    shortDescription: Yup.string()
      .min(20, 'Minino 20 caracteres')
      .required('Campo Obrigatório'),
    description: Yup.string()
      .min(30, 'Minino 30 caracteres')
      .required('Campo Obrigatório'),
    width: Yup.number(),
    heigth: Yup.number(),
    deep: Yup.number(),
    weigth: Yup.number(),
    code: Yup.number(),
    barCode: Yup.string().required('Campo Obrigatório'),
    category: Yup.string().required('Campo Obrigatório'),
    price: Yup.number().required('Campo Obrigatório'),
    dateInit: Yup.date()
      .max(Date(), `Maximo Hoje ${DateToDay()}`)
      .required('Campo Obrigatório'),
    imgUrl: Yup.string().required('Campo Obrigatório'),
    imgMini: Yup.string().required('Campo Obrigatório')
  })
  return (
    <Formik
      initialValues={{
        id: '',
        quantity: '',
        title: '',
        slug: '',
        shortDescription: '',
        description: '',
        width: '',
        heigth: '',
        deep: '',
        weigth: '',
        code: '',
        barCode: '',
        category: '',
        price: '',
        dateInit: '',
        imgUrl: '',
        imgMini: ''
      }}
      validationSchema={validate}
      onSubmit={(values) => console.log(values)}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting
      }) => {
        return (
          <FormProduct onSubmit={handleSubmit}>
            <RowForm>
              <ColForm>
                <FormControl>
                  <TextField name="id" label="ID" type="number" />
                </FormControl>
                <FormControl>
                  <TextField label="Titulo" name="title" />
                </FormControl>
                <FormControl>
                  <TextField label="Slug" name="slug" />
                </FormControl>
                <FormControl>
                  <TextField label="Largura (cm)" name="width" type="number" />
                </FormControl>
                <FormControl>
                  <TextField
                    label="Cumprimento (cm)"
                    name="heigth"
                    type="number"
                  />
                </FormControl>
                <FormControl>
                  <TextField label="Altura (cm)" name="deep" type="number" />
                </FormControl>
                <FormControl>
                  <TextField label="Peso (Kg)" name="weigth" type="number" />
                </FormControl>
              </ColForm>
              <ColForm>
                <FormControl>
                  <TextField label="Quantidade" name="quantity" type="number" />
                </FormControl>
                <FormControl>
                  <TextField label="Código" name="code" type="number" />
                </FormControl>
                <FormControl>
                  <TextField label="Código de barras" name="barCode" />
                </FormControl>
                <FormControl>
                  <TextField label="Categoria" name="category" />
                </FormControl>
                <FormControl>
                  <TextField label="Preço (US$)" name="price" type="number" />
                </FormControl>
                <FormControl>
                  <TextField
                    label="Fecha de entrada"
                    name="dateInit"
                    type="date"
                  />
                </FormControl>
              </ColForm>
              <ColForm>
                <FormControl>
                  <TextArea label="Descrição curta" name="shortDescription" />
                </FormControl>
                <FormControl>
                  <TextArea label="Descrição" name="description" />
                </FormControl>
                <FormControl>
                  <label htmlFor="novaimagem">Imagem</label>
                  <Field
                    className={errors.imgUrl ? 'error' : null}
                    id="novaimagem"
                    name="imgUrl"
                    type="file"
                  />
                  {touched.imgUrl && errors.imgUrl ? (
                    <div className="msnError">{errors.imgUrl}</div>
                  ) : null}
                </FormControl>
                <FormControl>
                  <label htmlFor="novaimagemMin">Imagem miniatura</label>
                  <Field
                    className={errors.imgMini ? 'error' : null}
                    id="novaimagemMin"
                    name="imgMini"
                    type="file"
                  />
                  {touched.imgMini && errors.imgMini ? (
                    <div className="msnError">{errors.imgMini}</div>
                  ) : null}
                </FormControl>
              </ColForm>
            </RowForm>
            <BtnSubmit disabled={Object.keys(errors).length > 0} type="submit">
              Salvar
            </BtnSubmit>
          </FormProduct>
        )
      }}
    </Formik>
  )
}

export default FormCreate
