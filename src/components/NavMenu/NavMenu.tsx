import Image from 'next/image'
import { BtnLogo, ContainerNav, TituloMenu } from './styles'

const NavMenu = () => {
  return (
    <ContainerNav>
      <BtnLogo href="/">
        <a>
          <Image
            src={'/images/svg/LogoSponteBranca.svg'}
            width={185}
            height={63}
          />
        </a>
      </BtnLogo>
      <TituloMenu>Controle de Estoque</TituloMenu>
    </ContainerNav>
  )
}

export default NavMenu
