import styled from 'styled-components'
import Link from 'next/link'

export const BtnLogo = styled(Link)`
  width: 185px;
  height: 63px;
`

export const ContainerNav = styled.header`
  display: flex;
  width: 100%;
  max-width: 1200px;
  margin: 1rem auto 0;
  padding: 2rem 1.5rem;
  border-radius: 9px 9px 0 0;
  border-bottom: 1px solid #f1f1f1;
  background-color: #76ccfe;
  align-items: center;
  @media (max-width: 480px) {
    padding: 1rem 0.5rem;
    margin: 0.5rem auto 0;
  }
`

export const TituloMenu = styled.h1`
  color: #fff;
  font-size: 30px;
  margin: 0 auto;
`
