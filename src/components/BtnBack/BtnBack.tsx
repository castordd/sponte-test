import { BtnLink, LinkBtn } from './styles'
import { ArrowToLeft } from '@styled-icons/boxicons-regular/ArrowToLeft'
const BtnBack = ({ url }) => {
  return (
    <BtnLink href={url}>
      <LinkBtn>
        <ArrowToLeft size={20} />
        Voltar
      </LinkBtn>
    </BtnLink>
  )
}

export default BtnBack
