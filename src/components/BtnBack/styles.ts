import styled from 'styled-components'
import Link from 'next/link'

export const BtnLink = styled(Link)``

export const LinkBtn = styled.a`
  color: #ccc;
  position: absolute;
  left: 1rem;
  top: 0.5rem;
  align-items: center;
  font-size: 14px;
  font-weight: bold;
  cursor: pointer;
  padding: 0.5rem;
  border-radius: 6px;
  transition: all ease-in-out 300ms;

  svg {
    margin: -3px 6px 0 0;
  }

  &:hover {
    color: #666;
  }
`
