import { Field, ErrorMessage, useField, FieldProps } from 'formik'

const FileField = ({ label, ...props }) => {
  const [field, { error, touched }, helper] = useField<FieldProps>(props)
  console.log(field)

  return (
    <>
      <label htmlFor={field.name}>{label}</label>
      <Field
        {...field}
        {...props}
        type="file"
        onChange={(e) => {
          helper.setValue(e.currentTarget.files[0])
        }}
        id={field.name}
      />
      {touched && (
        <ErrorMessage name={field.name}>
          {() => <div>{error}</div>}
        </ErrorMessage>
      )}
    </>
  )
}

export default FileField
