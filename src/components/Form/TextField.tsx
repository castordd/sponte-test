import { useField } from 'formik'

const TextField = ({ label, ...props }) => {
  const [field, meta] = useField(props)

  return (
    <>
      <label
        className={meta.error !== undefined ? 'error' : null}
        htmlFor={props.id || props.name}
      >
        {label}
      </label>
      <input
        className={meta.error !== undefined ? 'error' : null}
        {...field}
        {...props}
      />
      {meta.touched && meta.error ? (
        <div className="msnError">{meta.error}</div>
      ) : null}
    </>
  )
}

export default TextField
