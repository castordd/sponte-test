import { AlertData, SmileDog, LinkBack, BackLink } from './styles'
const AlertDog = () => {
  return (
    <AlertData>
      <p>Não foram encontrados dados para este producto.</p>
      <SmileDog
        width={250}
        height={250}
        loading="lazy"
        src="/images/smile-dog.jpeg"
      />
      <LinkBack href="/">
        <BackLink>Voltar</BackLink>
      </LinkBack>
    </AlertData>
  )
}

export default AlertDog
