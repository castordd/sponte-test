import styled from 'styled-components'

import Image from 'next/image'
import Link from 'next/link'

export const AlertData = styled.div`
  max-width: 30%;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  color: #666;
  font-size: 16px;
  text-align: center;
  padding: 2rem;
  border-radius: 9px;
  background-color: #fff;
  margin: 0 auto;
  box-shadow: 0 8px 24px -9px rgba(0, 0, 0, 0.5);
  p {
    margin: 0 0 1rem;
  }
`
export const SmileDog = styled(Image)``

export const LinkBack = styled(Link)``

export const BackLink = styled.a`
  display: inline-block;
  color: #fff;
  font-size: 16px;
  font-weight: bold;
  padding: 1rem 2rem;
  margin: 1rem auto 0;
  border-radius: 9px;
  background-color: #76ccfe;
  cursor: pointer;
  transition: all ease-in-out 300ms;

  &:hover {
    background-color: #6abae9;
    box-shadow: 0 8px 24px -9px rgba(0, 0, 0, 0.5);
  }
`
