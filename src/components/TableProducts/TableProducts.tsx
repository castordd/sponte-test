import Image from 'next/image'
import { PRODUCTS } from '../../data/products'
import { Plus } from '@styled-icons/fa-solid/Plus'
import { Container, BtnAdd, AddLink, Tableheader, Table } from './styles'
import Link from 'next/link'

const TableProducts = () => {
  return (
    <Container>
      <Tableheader>
        <h2>Lista de Produtos</h2>
        <BtnAdd href="/product/create">
          <AddLink className="btn">
            <Plus size="20" />
            <span>Novo Produto</span>
          </AddLink>
        </BtnAdd>
      </Tableheader>
      <Table>
        <thead>
          <tr>
            <th>id</th>
            <th>Quantidade</th>
            <th>Nome</th>
            <th className="descript">Descrição</th>
            <th>Imagem</th>
          </tr>
        </thead>
        <tbody>
          {PRODUCTS.map((prod: any, index: any) => {
            return (
              <Link key={index} href={`/product/${prod.slug}`}>
                <tr>
                  <td>{prod.id}</td>
                  <td className="quant">{prod.quantity}</td>
                  <td>
                    {prod.title}
                    <br />
                    <small># {prod.code}</small>
                  </td>
                  <td className="descript">{prod.shortDescription}</td>
                  <td>
                    <Image
                      src={prod.imgMini}
                      alt={prod.title}
                      loading="lazy"
                      width={100}
                      height={100}
                    />
                  </td>
                </tr>
              </Link>
            )
          })}
        </tbody>
      </Table>
    </Container>
  )
}

export default TableProducts
