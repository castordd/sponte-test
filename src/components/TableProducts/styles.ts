import styled from 'styled-components'
import Link from 'next/link'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-content: flex-end;
  max-width: 1200px;
  padding: 2rem 1rem;
  margin: 0 auto;
  border-radius: 0 0 9px 9px;
  border: 1px solid #eee;
  border-top: none;
  background-color: #f1f1f1;
`
export const Tableheader = styled.div`
  width: 100%;
  align-items: center;
  border-bottom: 1px solid #785afd;
  margin: 0 0 1rem;
  h2 {
    color: #785afd;
    display: inline-block;
    margin: 10px 0 0 1rem;
  }
`

export const BtnAdd = styled(Link)``

export const AddLink = styled.a`
  position: relative;
  width: 40px;
  height: 40px;
  display: flex;
  float: right;
  align-items: center;
  background-color: #785afd;
  border-radius: 6px 6px 0 0;
  cursor: pointer;
  clear: both;
  margin: 0 1.5rem 0 0;

  @media (max-width: 480px) {
    margin: 0 0.9rem 0 0;
  }

  svg {
    fill: #fff;
    width: 20px;
    margin: 0 auto;
    path {
      fill: #fff;
      opacity: 0.5;
    }
  }
  span {
    position: absolute;
    left: -50%;
    top: -10px;
    display: inline-block;
    width: max-content;
    color: #fff;
    font-size: 10px;
    padding: 0.3rem;
    border-radius: 3px;
    background-color: rgba(0, 0, 0, 0.5);
    opacity: 0;
    transition: all ease-in-out 300ms;
  }
  &:hover {
    svg {
      path {
        opacity: 1;
      }
    }
    span {
      top: -25px;
      opacity: 1;
    }
  }
`
export const Table = styled.table`
  width: 100%;
  border: none;
  border-spacing: 1px;
  background-color: #f1f1f1;

  tbody {
    &:hover {
      tr {
        opacity: 0.5;
        &:hover {
          opacity: 1;
          box-shadow: 0 8px 24px -9px #000;
        }
      }
    }
  }

  th,
  td {
    color: #666;
    font-size: 0.9rem;
    padding: 0.5rem 1rem;
    border-radius: 6px;
    background-color: #fff;
    small {
      color: #ccc;
      font-weight: bold;
    }
  }
  tr {
    cursor: pointer;
    transition: all ease-in-out 300ms;
  }

  .quant {
    color: #785afd;
    font-size: 16px;
    font-weight: bold;
    text-align: center;
  }
  @media (max-width: 480px) {
    th,
    td {
      font-size: 0.7rem;
      padding: 0.5rem;
    }
    .descript {
      display: none;
    }
  }
`
