declare namespace I {
  export interface ProductProps {
    id: number
    quantity: number
    title: string
    slug: string
    shortDescription: string
    description: string
    sizes: {
      width: number
      heigth: number
      deep: number
    }
    weigth: number
    code: number | string
    barCode: string
    category: string
    price: number
    dateInit: string
    imgUrl: string
    imgMini: string
  }
}
